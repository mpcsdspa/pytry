.. simple-test documentation master file, created by
   sphinx-quickstart on Sat Aug 18 00:30:52 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to simple-test's documentation!
=======================================

I'm Bharath, Humble Professor.

I've contributed to:

*   UMD
*   IIT-M
*   RCB

Overview on How to Run this API
================================
1. Either install a Python IDE or create a Python virtual environment to install the packages required
2. Install packages required
3. Install MySQL 5.7
4. Install Postman extension in Chrome or install curl


**************
Basics of Code
**************

.. toctree::
   :maxdepth: 2

   all-me


*********************************
The framework and library
*********************************

.. toctree::
   :maxdepth: 2

   pycode.cc


Indices and tables
==================


* :ref:`modindex`
* :ref:`search`
